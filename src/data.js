import car1 from "./images/model-1.jpg";
import car2 from "./images/model-2.jpg";
import car3 from "./images/model-3.jpg";
import car4 from "./images/model-4.jpg";
import img1 from "./images/car-1.jpg";
import img2 from "./images/car-2.jpg";
import img3 from "./images/car-3.jpg";
import img4 from "./images/car-4.jpg";
import img5 from "./images/car-5.jpg";
import img6 from "./images/car-6.jpg";
import img7 from "./images/car-7.jpg";
import img8 from "./images/car-8.jpg";
import img9 from "./images/car-9.jpg";
import img10 from "./images/car-10.jpg";
import img11 from "./images/car-11.jpg";
import img12 from "./images/car-12.jpg";

export default [
  {
    sys: {
      id: "1"
    },
    fields: {
      name: "Alfa Romeo",
      slug: "alfa-romeo-especial",
      type: "carreras",
      price: 100,
      gasoline: 'Diesel',
      capacity: 2,
      featured: false,
      //date:'2014/02/10',
      description:
      "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
    extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img1
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "2"
    },
    fields: {
      name: "Mercedes",
      slug: "mercedes",
      type: "carreras",
      price: 150,
      gasoline: 'Diesel',
      capacity: 5,
      featured: false,
      description:
      "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
    extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img2
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "3"
    },
    fields: {
      name: "Alfa Romeo",
      slug: "alfa-romeo",
      type: "carreras",
      price: 250,
      gasoline: 'Gasolina',
      capacity: 2,
      featured: false,
      description:
      "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
    extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img3
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "4"
    },
    fields: {
      name: "Audi",
      slug: "audi-especial",
      type: "carreras",
      price: 300,
      gasoline: 'Diesel',
      capacity: 2,
      featured: false,
      description:
      "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
    extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img4
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "5"
    },
    fields: {
      name: "Audi",
      slug: "audi",
      type: "familiar",
      price: 200,
      gasoline: 'Diesel',
      capacity: 5,
      featured: false,
      description:
      "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
    extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img5
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "6"
    },
    fields: {
      name: "BMW",
      slug: "bmw-1",
      type: "familiar",
      price: 250,
      gasoline: 'Gasolina',
      capacity: 4,
      featured: false,
      description:
      "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
    extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img6
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "7"
    },
    fields: {
      name: "Audi",
      slug: "audi-audi",
      type: "familiar",
      price: 300,
      gasoline: 'Diesel',
      capacity: 4,
      featured: false,
      description:
      "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
    extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img7
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "8"
    },
    fields: {
      name: "Jeep",
      slug: "jeep",
      type: "familiar",
      price: 400,
      gasoline: 'Gasolina',
      capacity: 2,
      featured: true,
      description:
      "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
    extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img8
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "9"
    },
    fields: {
      name: "Jeep",
      slug: "jeep-deluxe",
      type: "pickup",
      price: 300,
      gasoline: 'Gasolina',
      capacity: 5,
      featured: false,
      description:
        "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
      extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img9
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "10"
    },
    fields: {
      name: "Cadillac",
      slug: "cadillac",
      type: "pickup",
      price: 350,
      size: 550,
      gasoline: 'Gasolina',
      capacity: 2,
      featured: false,
      description:
        "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
      extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img10
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "11"
    },
    fields: {
      name: "Cadillac",
      slug: "cadillac-especial",
      type: "pickup",
      price: 400,
      gasoline: 'Gasolina',
      capacity: 4,
      featured: false,
      description:
      "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
    extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: img11
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "12"
    },
    fields: {
      name: "Chevrolet",
      slug: "chevrolet",
      type: "pickup",
      price: 500,
      gasoline: 'Gasolina',
      capacity: 2,
      featured: true,
      description:
      "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
    extras: [
        "CALEFACCIÓN EN LOS ASIENTOS",
        "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
        "CONECTIVIDAD CON BLUETOOTH",
        "ENCHUFES ​Y CONEXIONES",
        "ASIENTOS Y ​VOLANTE AJUSTABLES"
      ],
      images: [
        {
          fields: {
            file: {
              url: img12
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "13"
    },
    fields: {
      name: "Ferrari",
      slug: "Ferrari",
      type: "coupe",
      price: 600,
      gasoline: 'Gasolina',
      capacity: 4,
      featured: true,
      description:
        "Tu Beat HB se acelera con sólo pensar en el camino que está por delante. Las posibilidades infinitas que tiene el destino para ti son razón suficiente para continuar el viaje y descubrir eso que te mueve y te inspira a llegar más lejos. Expresa lo que sientes con el diseño y espacio necesario para seguir adelante en lo que sea que te propongas.",
      extras: [
          "CALEFACCIÓN EN LOS ASIENTOS",
          "CÁMARA TRASERA / SENSORES DE APARCAMIENTO",
          "CONECTIVIDAD CON BLUETOOTH",
          "ENCHUFES ​Y CONEXIONES",
          "ASIENTOS Y ​VOLANTE AJUSTABLES"
        ],
      images: [
        {
          fields: {
            file: {
              url: car1
            }
          }
        },
        {
          fields: {
            file: {
              url: car2
            }
          }
        },
        {
          fields: {
            file: {
              url: car3
            }
          }
        },
        {
          fields: {
            file: {
              url: car4
            }
          }
        }
      ]
    }
  }
];
