import React from 'react';
import {Link} from 'react-router-dom';
import defaultimg from '../images/car-1.jpg'
import PropTypes from 'prop-types';

export default function Car({ car }) {
    const{name, slug, images, price} = car;
    return (
      <article className='car'>
          <div className='img-container'>
              <img src={images[0] || defaultimg} alt="Carro" />
              <div className='price-bottom'>
                  <h6>${price}</h6>
                  <p>Por dia</p>
              </div>
              <Link to={`/Cars/${slug}`} 
              className='btn-primary car-link'>Destacado</Link>
          </div>
          <p className='car-info'>{name}</p>
      </article> 
    );
}

Car.prototype = {
    car:PropTypes.shape({
        name:PropTypes.string.isRequired,
        slug:PropTypes.string.isRequired,
        images:PropTypes.arrayOf(PropTypes.string).isRequired,
        price:PropTypes.number.isRequired,
    })

}