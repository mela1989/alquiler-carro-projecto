import React from 'react';
import CarFilter from './CarFilter';
import CarList from './CarList';
import {conConsumerCar} from '../context';
import Loading from './Loading';

function CarContainer({context}){
   const {loading, carsSorted, cars} = context;
   if(loading){
    return<Loading />
}
   return(
    <div>
    <CarFilter cars={cars} />
    <CarList cars={carsSorted}/>
     </div>
   );
}

export default conConsumerCar(CarContainer);



    