import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { TiThMenuOutline } from "react-icons/ti";
import logo from '../images/logo.png';

export default class Navbar extends Component {
    state={
        isOpen:false
    };
    handleToggle = () =>{
        this.setState({isOpen: !this.state.isOpen});
    };
    render() {
        return (
            <nav className="navbar">
                <div className='nav-center'>
                <div className='nav-header'>
                    <Link to='/'>
                        <img src={logo} alt='logo-cars' />
                    </Link>
                    <button 
                        type='button' 
                        className='nav-btn' 
                        onClick={this.handleToggle}
                        >
                        <TiThMenuOutline className='nav-icon'/>
                    </button>
                    </div>
                    <ul 
                        className={this.state.isOpen ? 'nav-links show-nav': 'nav-links'}
                        >
                        <li>
                            <Link to='/'>Home</Link>
                        </li>
                        <li>
                            <Link to='/cars'>Cars</Link>
                        </li>
                        <li>
                            <Link to='/login'>Login</Link>
                        </li>
                        <li>
                            <Link to='/signup'>Signup</Link>
                        </li>
                      
                        </ul>
                </div>
            </nav>
        );
    }
}
