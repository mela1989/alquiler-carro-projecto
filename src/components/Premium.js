import React from 'react';

export default function Premium({children, premium}) {
    return <header className={premium}>
       {children}
       </header>;
    
}

Premium.defaultProps = {
    premium: 'defaultPremium'
};
