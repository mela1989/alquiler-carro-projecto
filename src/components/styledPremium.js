import styled from 'styled-components';
import mainbg from '../images/mainbg.jpg';


const StyledPremium = styled.header`

min-height: 60vh;
background: url(${props => (props.img ? props.img : mainbg)}) right no-repeat;
display: flex;
align-items: center;
justify-content: left;
background-size: 800px 600px;
margin-left:80px;   
background-color:rgba(233, 233, 233, 0.692);

@media  (max-width:  768px) {
   
    background: url(${props => (props.img ? props.img : mainbg)}) center/cover no-repeat;
    display: flex;
    align-items: center;
    justify-content: center;
    background-size: auto;
    margin-left:0;  

}
`;



export default StyledPremium;

