import React, { Component } from 'react';
import {CarContext} from '../context';
import Loading from './Loading';
import Car from './Car';
import Title from './Titel';

export default class CarsFeatured extends Component {
    static contextType = CarContext;
    render(){
      let { loading, carsFeatured : cars}= 
        this.context;
        cars = cars.map(car => {
            return <Car key={car.id} car={car} />
        })
            
        return (
            <section className='featured-cars'>
                <Title title='Nuestros mejores carros' />
                <div className='featured-cars-center'>
                    {loading ? <Loading /> : cars}
                </div>

            </section>
        );
        
    }
}
