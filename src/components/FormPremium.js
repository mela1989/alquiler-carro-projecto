import React from 'react';


export default function FormPremium({children, title, formPremium}) {
    return(
   <div>
    <header className={formPremium}>
        <h1>{title}</h1>
       {children}
       </header>
       </div>
    );
    
}

FormPremium.defaultProps = {
    formPremium: 'formPremium'

};

