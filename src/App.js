import React from 'react';
import './App.css';
import { Switch, Route } from "react-router-dom";

//pages
import Home from './pages/Home';
import Cars from './pages/Cars';
import SingleCar from './pages/SingleCar';
import Error from './pages/Error';
import login from './pages/login';
import signup from './pages/signup';

//components
import Navbar from './components/Navbar';
import AuthRoute from './ui/AuthRoute';

import createMuiTheme from '@material-ui/core/styles/createMuiTheme';


import jwtDecode from 'jwt-decode';



let authenticaded;
const token = localStorage.FirebaseIdToken;
if (token){
  const decodedToken =jwtDecode(token);
  console.log(decodedToken);
}

function App() {
  return (
      <>
      <Navbar />
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/cars/' component={Cars} />
          <Route exact path='/cars/:slug' component={SingleCar} />
          <AuthRoute exact path='/login/' component={login} authenticaded={authenticaded}/>
          <AuthRoute exact path='/signup/' component={signup} authenticaded={authenticaded}/>
          <Route component={Error} />
        </Switch>
     
     </>
  )
}

export default App;
