import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
//import { MdCenterFocusStrong } from 'react-icons/md';
import { CircularProgress } from '@material-ui/core';

const styles = {
    form:{
        textAlign:'center',
        marginTop:'10rem'
    },
    PageTitle:{
        marginTop: '20px auto 20px auto'
    },
    textField: {
        margin: '10px auto 10px auto'
        
    },
    buttonForm:{
        marginTop: 20,
        marginBottom: 50,
        position:'relative'
    },
    customError:{
        color: 'red',
        fontSize: '0.8rem',
        marginTop: '10px'
    },
    progress:{
        position:'absolute'
    }
}

class signup extends Component {
    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            handle: '',
            loadingUser:false,
            errors: {}
        };
    }

    handleSubmit= (event) => {
        event.preventDefault();
        this.setState({
            loadingUser: true
        });
        const newUserData = {
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword,
            handle: this.state.handle
        };
        axios
            .post('/signup', newUserData)
            .then((res) => {
            console.log(res.data);
            localStorage.setItem('FirebaseIdToken', `Bearer ${res.data.token}`);
            this.setState({
                loadingUser: false
            });
         
            this.props.history.push('/');
        })
        .catch((err) => {
            this.setState({
                errors: err.response.data,
                loadingUser:false
            });
        });
    };
    handleChange= (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    render() {
        const {classes} = this.props;
        const {errors, loadingUser } = this.state;

        return (
            <Grid container className={classes.form}>
            <Grid item sm />
            <Grid item sm>
              <Typography variant="h2" className={classes.pageTitle}>
                Signup
              </Typography>
              <form noValidate onSubmit={this.handleSubmit}>
                <TextField
                  id="email"
                  name="email"
                  type="email"
                  label="Email"
                  className={classes.textField}
                  helperText={errors.email}
                  error={errors.email ? true : false}
                  value={this.state.email}
                  onChange={this.handleChange}
                  fullWidth
                />
                <TextField
                  id="password"
                  name="password"
                  type="password"
                  label="Contraseña"
                  className={classes.textField}
                  helperText={errors.password}
                  error={errors.password ? true : false}
                  value={this.state.password}
                  onChange={this.handleChange}
                  fullWidth
                />
                <TextField
                  id="confirmPassword"
                  name="confirmPassword"
                  type="password"
                  label="Confirmar Contraseña"
                  className={classes.textField}
                  helperText={errors.confirmPassword}
                  error={errors.confirmPassword ? true : false}
                  value={this.state.confirmPassword}
                  onChange={this.handleChange}
                  fullWidth
                />
                <TextField
                  id="handle"
                  name="handle"
                  type="text"
                  label="Handle"
                  className={classes.textField}
                  helperText={errors.handle}
                  error={errors.handle ? true : false}
                  value={this.state.handle}
                  onChange={this.handleChange}
                  fullWidth
                />
                {errors.general && (
                 <Typography variant="body2" className={classes.customError}>
                 {errors.general}
               </Typography>
                )}
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.buttonForm}
                  disabled={loadingUser}
                >
                  Signup
                  {loadingUser && <CircularProgress size={30} className={classes.progress} />}
                </Button>
                <br />
                <small>
                  Ya tienes tu cuenta? Dirgete al login <Link to="/login">here</Link>
                </small>
              </form>
            </Grid>
            <Grid item sm />
          </Grid>
                   
                   
         );
    }
}

signup.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(signup);


