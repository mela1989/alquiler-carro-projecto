import React, {Component} from 'react';
import axios from 'axios';
import Premium from '../components/Premium';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';
import Services from '../components/Services';
import CarsFeatured from '../components/CarsFeatured';


class Home extends Component  {
    state = {
        cars: null
    }
       componentDidMount(){
        axios.get('/cars')
            .then((res)=> {
                console.log(res.data)
                this.setState({
                    cars: res.data
                })
            })
            .catch((err)=> console.log(err));
       }

    render() {
        
         let recentCars = this.state.cars ? (
             this.state.cars.map((car) => <p>{car.title}</p>)
         ) : (
           <p> loading</p>
         );
        return(
            <>
            <Premium>
            <Banner 
                title={recentCars}
                subtitle='Alquila un carro en el mejor precio'>
                    <Link to='/cars' className='btn-primary'>
                        Carros disponibles
                    </Link>
            </Banner>            
        </Premium>
        <Services />
        <CarsFeatured />
        </>
        );
        }
    }

    export default Home;


