import React from 'react'
import Premium from '../components/Premium';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';

export default function Error() {
    return <Premium>
        <Banner 
            title='404'
            subtitle='Esta pagina no existe'
            >
                <Link to='/' className='btn-primary'>
                Volver al Home
                </Link>
            </Banner>
    </Premium>
}
