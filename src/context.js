import React, { Component } from 'react';
import items from './data';

const CarContext = React.createContext();

export default class CarProvider extends Component {
    state= {
        cars:[],
        carsSorted:[],
        carsFeatured:[],
        loading:true,
        type:'all',
        capacity:1,
        price:0,
        minPrice:0,
        maxPrice:0,
       // date:'2019/02/10',
        //startDate:0,
       // endDate:0
    };

    

    componentDidMount(){
        let cars = this.formatData(items);
        let carsFeatured = cars.filter(car => car.featured === true);
        let maxPrice = Math.max(...cars.map(item =>item.price));
       
    this.setState({
        cars,
        carsFeatured,
        carsSorted:cars,
        loading: false,
        price: maxPrice,
        maxPrice,
       // date: startDate,
       // startDate

    });
    }

    formatData(items){
        let tempItems = items.map(item => {
            let id = item.sys.id;
            let images = item.fields.images.map(image => image.fields.file.url);
            
            let car = {...item.fields, images, id};
            return car;
            });
            return tempItems;
        }

        getCar = slug => {
            let tempCars = [...this.state.cars];
            const car = tempCars.find(car => car.slug === slug);
            return car;
        };
    
        handleChange = event => {
            const target = event.target;
            const value = target.type === "checkbox" ? target.checked : target.value;
            const name = target.name;
            console.log(name, value);
        
            this.setState(
              {
                [name]: value
              },
              this.filterCars
            );
          };

          filterCars= () => {
            let {
              cars,
              type,
              capacity,
              price,
              //date
              
            } = this.state;
        
            let tempCars = [...cars];
          
            capacity = parseInt(capacity);
            price = parseInt(price);
           
           
            if (type !== "all") {
              tempCars = tempCars.filter(car => car.type === type);
            }
           
            if (capacity !== 1) {
              tempCars = tempCars.filter(car => car.capacity >= capacity);
            }
            
            tempCars = tempCars.filter(car => car.price <= price);

           
            
            this.setState({
                carsSorted: tempCars
            });
          };

    render() {
        return (
            <CarContext.Provider 
            value={{
                ...this.state,
                getCar: this.getCar,
                handleChange: this.handleChange
              }}
              >
                  {this.props.children}
            </CarContext.Provider>
        );
    }
    }


const CarConsumer = CarContext.Consumer;

export { CarProvider, CarConsumer, CarContext };


export function conConsumerCar(Component){
    return function wrapperConsumerCar(props){
        return(
         <CarConsumer>
            {value => <Component {...props} context={value} />}
        </CarConsumer>
    );
};

}