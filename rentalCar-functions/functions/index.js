
const functions = require('firebase-functions');
const app = require('express')();
const firebaseAuth = require('./util/firebaseAuth');
const { getAllCars, postOneCar } = require('./handlers/cars');
const { signup, login } = require('./handlers/users');




//cars routes
app.get('/cars', getAllCars);
app.post('/cars', firebaseAuth, postOneCar); 


//  User route
app.post('/signup', signup);
app.post('/login', login);



exports.api = functions.https.onRequest(app);

