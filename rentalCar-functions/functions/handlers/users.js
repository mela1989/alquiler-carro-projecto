const { db } = require('../util/admin');

const firebaseConfig = require('../util/firebaseConfig');
const firebase = require('firebase');
firebase.initializeApp(firebaseConfig);

const { validateSignupData, validateLoginData } = require('../util/validators');

exports.signup = (req, res)=> {
    const newUser = {
        email: req.body.email,
        password: req.body.password,
        confirmPassword: req.body.confirmPassword,
        handle: req.body.handle,
  };

const {valid, errors} = validateSignupData( newUser );

if(!valid) return res.status(400).json(errors);

//validacion de data
   let token, userId;
    db.doc(`/users/${newUser.handle}`).get()
    .then((doc)=> {
        if(doc.existe){
            return res.status(400).json({handle: 'este handle ya esta registrado'});
        } else {
            return firebase
            .auth()
            .createUserWithEmailAndPassword(newUser.email, newUser.password);
        }
    })
    .then((data) => {
        userId = data.user.uid;
        return data.user.getIdToken();
    })
    .then((idToken) =>{
        token = idToken;
        const userCredentials = {
            handle:newUser.handle,
            email:newUser.email,
            createdAt: new Date().toISOString(),
            userId
        };
        return db.doc(`/users/${newUser.handle}`).set(userCredentials);
    })
    .then(() => {
        return res.status(201).json({token});
    })
    .catch(err => {
        console.error(err);
        if(err.code === "auth/email-already-in-use"){
            return res.status(400).json({email: 'El e-mail ya existe'});
        } else { 
        return res.status(500).json({ error: err.code});
        }
    });
}

exports.login =  (req, res) => {
    const user = {
        email: req.body.email,
        password: req.body.password
    };
 
    const { valid, errors } = validateLoginData(user);

    if(!valid) return res.status(400).json(errors);
 
    firebase
     .auth()
     .signInWithEmailAndPassword(user.email, user.password)
     .then((data) => {
         return data.user.getIdToken();
     })
     .then((token) => {
          return res.json({token});
     })
     .catch((err) => {
         console.error(err);
         if(err.code === "auth/wrong-password"){
             return res.status(403).json({general: 'la contraseña esta incorrecta, intente de nuevo'});
         } else { 
         return res.status(500).json({ error: err.code});
         }
     });
 }