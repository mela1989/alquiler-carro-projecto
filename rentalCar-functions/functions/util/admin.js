const admin = require('firebase-admin');
const firestore =require('@google-cloud/firestore');


const serviceAccount = require('./rentalcar-9705c-firebase-adminsdk-od4fd-2e9f40cdff.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://rentalcar-9705c.firebaseio.com"
});

/*const firebase = require('firebase');
firebase.initializeApp(firebase);*/

const db = admin.firestore();

module.exports = {admin, db, serviceAccount, firestore};