    
const isEmail = (email)=>{
    const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(email.match(regEx)) return true;
    else return false;
};

const isEmpty = (string) => {
    if(string.trim() === '') return true;
    else return false;
};

exports.validateSignupData = (data) => {
    let errors = {};

//E-mail validation
if(isEmpty(data.email)){
  errors.email = 'No puede estar vacio';
} else if(!isEmail(data.email)){
  errors.email = 'Tiene que ser un email valido';
}

//Password validate
if(isEmpty(data.password)) errors.password = 'No puede estar vacio';
if(data.password !== data.confirmPassword) errors.confirmPassword = 'La contraseña debe concidir';  

//Handle validate
if(isEmpty(data.handle)) errors.handle = 'No puede estar vacio';

return{
    errors,
    valid: Object.keys(errors).length === 0 ? true :false
}
};

exports.validateLoginData = (data) => {
    let errors = {};

   if(isEmpty(data.email)) errors.email = 'No puede estar vacio';
   if(isEmpty(data.password)) errors.password = 'No puede estar vacio';

   return{
    errors,
    valid: Object.keys(errors).length === 0 ? true :false
}
};